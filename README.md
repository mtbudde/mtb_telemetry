MTB_Telemetry

A telemetry system designed to replicate the functions of many telemetry systems
that are used at the pro level. This system is intended to be less bulky than
the majority of the systems that are used currently while still maintaining affordability and accuracy.

The system will be based on a PIC32 microcontroller and will log all data to a
microSD card for analysis after a run.

Planned Functions:
GPS Tracking
Orientation Tracking
Environment Monitoring
Brake Usage Monitoring
Travel Monitoring (Front and Rear)

This will be a modular system with the possibility for future expansion and
upgrades.

All software will be based on the GPLv3.0 license.

For more information or if there is interest in collaborating on this project,
contact matthew.budde.mb@gmail.com
